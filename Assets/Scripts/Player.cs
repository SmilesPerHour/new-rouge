﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player : MovingObject {

	public Text HealthText;
	public AudioClip movementSound1;
	public AudioClip movementSound2;
	public AudioClip chopSound1;
	public AudioClip chopSound2;
	public AudioClip fruitSound1;
	public AudioClip fruitSound2;
	public AudioClip sodaSound1;
	public AudioClip sodaSound2;

	private Animator animator;
	private int playerHealth;
	private int attackPower = 1;
	private int healthPerFruit =5;
	private int healthPerSoda =10;
	private int secondsUntilNextlevel = 1;

	protected override void Start()
	{
		base.Start ();
		animator = GetComponent<Animator> ();
		playerHealth = GameController.Instance.playerCurrentHealth;
		HealthText.text = "Health:" + playerHealth;
	}

	private void OnDisable(){
		GameController.Instance.playerCurrentHealth = playerHealth;
	}

	void Update () {
		

		if (!GameController.Instance.isPlayerTurn) {
			return;
		}

		CheckIfGameOver ();

		int xAxis = 0;
		int yAxis = 0;

		xAxis = (int)Input.GetAxisRaw("Horizontal");
		yAxis = (int)Input.GetAxisRaw("Vertical");
		if (xAxis != 0) {
			yAxis =0;
		}
		if (xAxis != 0 || yAxis != 0) {
			playerHealth--;
			HealthText.text = "Health" + playerHealth;
			Move<Wall>(xAxis, yAxis);
			SoundController.Instance.PlaySingle (movementSound1, movementSound2);
			GameController.Instance.isPlayerTurn = false;
		}

	}

	private void OnTriggerEnter2D(Collider2D objectPlayerCollideWidth)
	{
		if (objectPlayerCollideWidth.tag == "Exit") {
			Invoke ("LoadNewLevel", secondsUntilNextlevel);
			enabled = false;
			
		} else if (objectPlayerCollideWidth.tag == "Fruit") {
			playerHealth += healthPerFruit;
			HealthText.text = "+" + healthPerFruit + "Health\n" + "Health: " + playerHealth;
			objectPlayerCollideWidth.gameObject.SetActive (false);
			SoundController.Instance.PlaySingle (fruitSound1, fruitSound2);
			
		} else if (objectPlayerCollideWidth.tag == "Soda") {
			playerHealth += healthPerSoda;
			HealthText.text = "+" + healthPerSoda + "Health\n" + "Health: " + playerHealth;
			objectPlayerCollideWidth.gameObject.SetActive (false);
			SoundController.Instance.PlaySingle (sodaSound1, sodaSound2);
		}
	}

	private void LoadNewLevel()
	{
		Application.LoadLevel (Application.loadedLevel);
	}

	protected override void HandleCollision<T>(T component)
	{
		Wall wall = component as Wall;
		animator.SetTrigger ("playerAttack");
		wall.DamageWall (attackPower);
	}
	public void TakeDamage(int damageReceived)
	{
		playerHealth -= damageReceived;
		HealthText.text = " - " + damageReceived + " Health\n" + playerHealth;
		animator.SetTrigger ("playerHurt");
	}
	private void CheckIfGameOver()
	{
		if (playerHealth <= 0) {
			GameController.Instance.GameOver ();
		}
	}
}
