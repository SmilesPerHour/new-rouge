﻿using UnityEngine;
using System.Collections;

public class Wall : MonoBehaviour {

	public int hitPoints = 2;
	public Sprite damagedWallSprite;
	private SpriteRenderer spriteRenderer;

	// Use this for initialization
	void Start () {
	
	}
	public void DamageWall(int damageRecieved)
	{
		hitPoints -= damageRecieved;
		spriteRenderer.sprite = damagedWallSprite;
		if (hitPoints <= 0)
		{
			gameObject.SetActive(false);
		}
	}
	

}
